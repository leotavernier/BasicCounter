﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounter
{
    public class Compteur
    {
        private int Valeur;

        public Compteur()
        {
            this.Valeur = 0;
        }

        public int Incrémentation()
        {
            Valeur++;
            return this.Valeur;
        }

        public int Décrementation()
        {
            if (Valeur == 0)
            {
                return this.Valeur;
            }
            else
            {
                Valeur--;
                return this.Valeur;
            }
            
        }

        public int RAZ()
        {
            Valeur = 0;
            return this.Valeur;
        }

        public int GetCompteur()
        {
            return this.Valeur;
        }
    }
}
