﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounter;

namespace TestBasicCounter
{
    [TestClass]
    public class TesBasicCounter
    {
        [TestMethod]
        public void TestIncrémentation()
        {
            Compteur Vlr = new Compteur();
            Assert.AreEqual(1, Vlr.Incrémentation());
        }

        [TestMethod]
        public void TesDécrementation()
        {
            Compteur Vlr = new Compteur();
            Assert.AreEqual(1, Vlr.Incrémentation());
            Assert.AreEqual(0, Vlr.Décrementation());
        }

        [TestMethod]
        public void TestRAZ()
        {
            Compteur Vlr = new Compteur();
            Assert.AreEqual(1, Vlr.Incrémentation());
            Assert.AreEqual(0, Vlr.RAZ());

        }
    }
}
