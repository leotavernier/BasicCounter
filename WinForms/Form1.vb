﻿Imports BasicCounter

Public Class Form1

    Dim compteur = New Compteur

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label_Valeur.Text = compteur.GetCompteur()
    End Sub


    Private Sub Btn_Décrementation_Click(sender As Object, e As EventArgs) Handles Btn_Décrementation.Click
        compteur.Décrementation()
        Label_Valeur.Text = compteur.GetCompteur()
    End Sub

    Private Sub Btn_Incrémentation_Click(sender As Object, e As EventArgs) Handles Btn_Incrémentation.Click
        compteur.Incrémentation()
        Label_Valeur.Text = compteur.GetCompteur()
    End Sub

    Private Sub Btn_RAZ_Click(sender As Object, e As EventArgs) Handles Btn_RAZ.Click
        compteur.RAZ()
        Label_Valeur.Text = compteur.GetCompteur()
    End Sub

End Class
