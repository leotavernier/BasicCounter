﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_Incrémentation = New System.Windows.Forms.Button()
        Me.Btn_Décrementation = New System.Windows.Forms.Button()
        Me.Btn_RAZ = New System.Windows.Forms.Button()
        Me.Lbl_Compteur = New System.Windows.Forms.Label()
        Me.Label_Valeur = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Btn_Incrémentation
        '
        Me.Btn_Incrémentation.Location = New System.Drawing.Point(462, 155)
        Me.Btn_Incrémentation.Name = "Btn_Incrémentation"
        Me.Btn_Incrémentation.Size = New System.Drawing.Size(139, 82)
        Me.Btn_Incrémentation.TabIndex = 0
        Me.Btn_Incrémentation.Text = "+"
        Me.Btn_Incrémentation.UseVisualStyleBackColor = True
        '
        'Btn_Décrementation
        '
        Me.Btn_Décrementation.Location = New System.Drawing.Point(183, 155)
        Me.Btn_Décrementation.Name = "Btn_Décrementation"
        Me.Btn_Décrementation.Size = New System.Drawing.Size(139, 82)
        Me.Btn_Décrementation.TabIndex = 1
        Me.Btn_Décrementation.Text = "-"
        Me.Btn_Décrementation.UseVisualStyleBackColor = True
        '
        'Btn_RAZ
        '
        Me.Btn_RAZ.Location = New System.Drawing.Point(323, 247)
        Me.Btn_RAZ.Name = "Btn_RAZ"
        Me.Btn_RAZ.Size = New System.Drawing.Size(139, 82)
        Me.Btn_RAZ.TabIndex = 2
        Me.Btn_RAZ.Text = "RAZ"
        Me.Btn_RAZ.UseVisualStyleBackColor = True
        '
        'Lbl_Compteur
        '
        Me.Lbl_Compteur.AutoSize = True
        Me.Lbl_Compteur.Location = New System.Drawing.Point(369, 155)
        Me.Lbl_Compteur.Name = "Lbl_Compteur"
        Me.Lbl_Compteur.Size = New System.Drawing.Size(31, 13)
        Me.Lbl_Compteur.TabIndex = 3
        Me.Lbl_Compteur.Text = "Total"
        '
        'Label_Valeur
        '
        Me.Label_Valeur.AutoSize = True
        Me.Label_Valeur.Location = New System.Drawing.Point(369, 190)
        Me.Label_Valeur.Name = "Label_Valeur"
        Me.Label_Valeur.Size = New System.Drawing.Size(0, 13)
        Me.Label_Valeur.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label_Valeur)
        Me.Controls.Add(Me.Lbl_Compteur)
        Me.Controls.Add(Me.Btn_RAZ)
        Me.Controls.Add(Me.Btn_Décrementation)
        Me.Controls.Add(Me.Btn_Incrémentation)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btn_Incrémentation As Button
    Friend WithEvents Btn_Décrementation As Button
    Friend WithEvents Btn_RAZ As Button
    Friend WithEvents Lbl_Compteur As Label
    Friend WithEvents Label_Valeur As Label
End Class
